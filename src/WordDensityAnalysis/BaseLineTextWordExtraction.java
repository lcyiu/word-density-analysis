package WordDensityAnalysis;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class BaseLineTextWordExtraction extends FeatureExtraction {
	
	private final String filename = "stopwords.txt";
	private HashSet<String> stopwords;
	private HashMap<String, Double> wordSet;

	public BaseLineTextWordExtraction(String u) 
	{
		super(u);
		wordSet = new HashMap<String, Double>();
	}

	//main parse function
	//first, get html document
	//loop through all elements in document and for each element
	//extract text features from each element
	//add score for each text feature 
	@Override
	public void parse() throws Exception 
	{
		Document doc = htmlParser.getDocument(0);
		stopwords = WordParser.parseWord(filename);
		System.out.println("Parsing html Document");
		
		Elements elements = doc.getAllElements();
		int weight =elements.size();
		int i = 0;
		for (Element element: elements)
		{
			if (element.hasText() && !element.ownText().equals(""))
			{
				//score: top of page get higher score than bottom of page
				double score = (weight - i++) / (weight * 1.0);
				HashSet<String> words = textExtraction(element.ownText());
				for (String word: words)
				{
					double s = wordSet.containsKey(word) ? wordSet.get(word) : 0;
					wordSet.put(word, s + 1 * score);
				}
			}
		}
	}
	
	//for each word in the sentence
	//remove all punctuation
	//if word is not a stopword, add word into a temporary string
	//add temporary string into word set if it is a valid word (isWord)
	//add each word into word set if it is a valid word (isWord)(word is not a stopword)
	//if word is a stopword
	//add add temporary string into word set if it is a valid word (isWord)
	private HashSet<String> textExtraction(String text)
	{
		HashSet<String> words = new HashSet<String>();
		text = processWord(text);
        Scanner in = new Scanner(text);
        String temp = "";
        while (in.hasNext()) 
        {
        	String word = in.next();
        	
        	//add temp to hashset and clear temp if word is a stopword
			if (isStopWord(word))
			{
				if (isWord(temp))
				{
					words.add(temp);
				}
				
				temp= "";
				continue;
			}
			
			//add individual word into hashset
			if (isWord(word))
			{
				words.add(word);
			}
			
			if (temp.length() > 0)
			{
				temp+=" ";
			}
			
			temp+=	word;
        }
        
        //add remaining word in temp into hashset
		if (isWord(temp))
		{
			words.add(temp);
		}
        
		return words;
	}
	
	//check if is a stop word, useful when element is a typical paragraph/article
	private boolean isStopWord(String word)
	{
		return stopwords.contains(word);
	}
	
	//sort word set by value and return the keyset
	@Override
	public Set<String> getFeatures() 
	{
		return MapUtil.sortByValues(wordSet).keySet();
	}
	
	//useful function to print all elements inside wordset
	@Override
	public void printFeatures() 
	{
		for (Entry<String, Double> entry : wordSet.entrySet()) 
		{
		    String key = entry.getKey();
		    Object value = entry.getValue();
		    System.out.println("Key: " + key + " Value: " + value);
		}
	}
}
