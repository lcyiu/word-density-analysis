package WordDensityAnalysis;

import java.util.Set;

public abstract class FeatureExtraction {
	
	protected HTMLParser htmlParser;
	
	public FeatureExtraction(String u)
	{
		htmlParser = new HTMLParser(u);
	}
	
	//Remove all punctuation
	protected String processWord(String word) 
	{
		return word.replaceAll("[](){}`\"$,.:;!?<>%]", "").toLowerCase();
	}
	
	//check if given string is a word
	//it is a word only if it contains at least one alphabet and the length is greater than 1
	protected boolean isWord(String s) 
	{
		return s.length() > 1 && s.matches(".*[a-zA-Z]+.*");
	}
	
	//Abstraction in case we have different feature extraction algorithm 
	public abstract void parse() throws Exception;
	public abstract Set<String> getFeatures();
	public abstract void printFeatures();
}
