package WordDensityAnalysis;

import java.io.IOException;

public class cmdLine 
{
	//check all arguments
	static void checkArgs(String[] args, StringBuilder url, Integer top, ExtractionMode mode)
	{
		try 
		{	
			if (args.length <2)
			{
				throw new IOException(ErrorHandler.argInvalid);
			}
			
			url.append(args[0]);
			top = Integer.parseInt(args[1]);
			
			if (args.length > 2)
			{
				if (args[2].equals("-n"))
				{
					mode = ExtractionMode.Normal;
				}
				else
				{
					String ErrorStr = String.format(ErrorHandler.modeInvalid, args[2]);
					throw new IOException(ErrorStr);
				}
			}
		}
		catch (Exception e)
		{
			ErrorHandler.printErrAndExit(e, ErrorHandler.usage);
		}
	}
}
