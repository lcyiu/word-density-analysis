package WordDensityAnalysis;

import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class HTMLParser {

	private URL url;
	
	//constructor 
	//validate url and return exception if error happen
	public HTMLParser(String u)
	{
		try
		{
			url = new URL(u);
		}
		catch (Exception e)
		{
			String ErrorStr = String.format(ErrorHandler.urlInvalid, u);
			ErrorHandler.printErrAndExit(e, ErrorStr);
		}
	}
	
	//attempt to get html document
	//there maybe timeout so there will be multiple attempts
	//return exception if error happens
	public Document getDocument(int attempt) throws Exception
	{
		try
		{
			System.out.println("Attempts to get html Documents: " + (attempt + 1));
			Document res =  Jsoup.connect(url.toString()).get();
			return res;
		}
		catch (Exception e)
		{
			if (attempt < 5)
			{
				return getDocument(attempt + 1);
			}
			else
			{
				ErrorHandler.printErrAndExit(e, ErrorHandler.tooManyAttemptError);
				return null;
			}
		}
	}
}
