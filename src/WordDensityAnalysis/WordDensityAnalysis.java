package WordDensityAnalysis;

import java.util.Iterator;

//different extraction mode for factory pattern
enum ExtractionMode 
{
	Normal
}

public class WordDensityAnalysis 
{		
	private FeatureExtraction featureExtraction;
	
	public WordDensityAnalysis(String url, ExtractionMode mode)
	{
		featureExtraction = createFeatureExtraction(url, mode);
	}
	
	//factory pattern for creating different text extraction algorithm
	private FeatureExtraction createFeatureExtraction(String url, ExtractionMode mode) {
		
		switch(mode)
		{
		case Normal:
			return new BaseLineTextWordExtraction(url);
		default:
			return new BaseLineTextWordExtraction(url);
		}
	}

	//parse document from given url
	public void parse()
	{
		try 
		{
			featureExtraction.parse();
		} catch (Exception e) 
		{
			ErrorHandler.printErrAndExit(e, ErrorHandler.parseError);
		}
	}
	
	public void printCommonTopics(int top)
	{
		int i = 0;
		Iterator<String> iter = featureExtraction.getFeatures().iterator();
		String topicStr= String.format("List of top %d common topics that best discribe the content of the page: \n", top);
		while (iter.hasNext() && i < top)
		{
			topicStr += iter.next() + "\n";
			i++;
		}
		
		System.out.println(topicStr);
	}
	
	public static void main(String[] args)
	{
		StringBuilder url = new StringBuilder();
		ExtractionMode em = ExtractionMode.Normal;
		
		cmdLine.checkArgs(args, url, new Integer(0), em);
		WordDensityAnalysis wda= new WordDensityAnalysis(url.toString(), em);
		wda.parse();
		wda.printCommonTopics(Integer.parseInt(args[1]));
	}
}
