package WordDensityAnalysis;

public class ErrorHandler {
	public static final String usage = "Usage: java -jar WordDensityAnalysis.jar \"url\" k (mode)\n"
			+ "Common Topic\n"
			+ "\tk\t\tTop k(integer) common topics\n"
			+ "mode (Extraction Mode)(optional)\n"
			+ "\t-n(default)\tNormal text extration\n";
	
	public static final String urlInvalid = "Error: url \"%S\" is invalid";
	
	public static final String argInvalid = "Error: Not enough arguments";
	
	public static final String modeInvalid = "Error: Extraction mode \"%s\" is invalid";
	
	public static final String parseError = "Error: Exception during parsing document";
	
	public static final String tooManyAttemptError = "Error: too many attempts trying to get document";
	
	public static void printErrAndExit(Exception e, String extraMsg)
	{
		System.out.println(extraMsg);
		System.out.println(e.getMessage());
		System.exit(-1);
	}
}
