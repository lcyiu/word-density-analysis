package WordDensityAnalysis;

import java.io.FileReader;
import java.util.HashSet;
import java.util.Scanner;

public class WordParser {
	//parse word file into set and return
	public static HashSet<String> parseWord(String filename) throws Exception
	{
		System.out.println("Parsing word document: " + filename);
		HashSet<String> words = new HashSet<String>();
		Scanner in = new Scanner(new FileReader(filename));
		while (in.hasNext())
		{
			words.add(in.next());
		}

		return words;
	}
}
